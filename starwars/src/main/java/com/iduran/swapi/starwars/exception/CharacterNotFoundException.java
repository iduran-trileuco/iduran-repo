package com.iduran.swapi.starwars.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Character Not Found")
public class CharacterNotFoundException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private final String details;

    public CharacterNotFoundException(String details) {
        super();
        this.details = details;
    }

    public String getDetails() {
        return details;
    }

}