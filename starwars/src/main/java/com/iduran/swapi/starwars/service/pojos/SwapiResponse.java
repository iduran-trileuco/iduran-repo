package com.iduran.swapi.starwars.service.pojos;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SwapiResponse {
    private final List<StarWarsSwapiCharacter> results;

    public SwapiResponse(List<StarWarsSwapiCharacter> results) {
        super();
        this.results = results;
    }

    public List<StarWarsSwapiCharacter> getResults() {
        return results;
    }

    @JsonCreator
    public static SwapiResponse build(@JsonProperty("results") List<StarWarsSwapiCharacter> results) {
        return new SwapiResponse(results);
    }
}