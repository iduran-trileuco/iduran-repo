package com.iduran.swapi.starwars.service.pojos;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class StarWarsSwapiCharacter {

    private final String name;
    private final String birthYear;
    private final String gender;
    private final String hairColor;
    private final String height;
    private final String mass;
    private final String skinColor;
    private final String homeWorld;
    private final List<String> films;
    private final List<String> starships;
    private final List<String> vehicles;

    public StarWarsSwapiCharacter(String name, String birthYear, String gender, String hairColor, String height,
            String mass, String skinColor, String homeWorld, List<String> films, List<String> starships,
            List<String> vehicles) {
        super();
        this.name = name;
        this.birthYear = birthYear;
        this.gender = gender;
        this.hairColor = hairColor;
        this.height = height;
        this.mass = mass;
        this.skinColor = skinColor;
        this.homeWorld = homeWorld;
        this.films = films;
        this.starships = starships;
        this.vehicles = vehicles;
    }

    public String getName() {
        return name;
    }

    public String getBirthYear() {
        return birthYear;
    }

    public String getGender() {
        return gender;
    }

    public String getHairColor() {
        return hairColor;
    }

    public String getHeight() {
        return height;
    }

    public String getMass() {
        return mass;
    }

    public String getSkinColor() {
        return skinColor;
    }

    public String getHomeWorld() {
        return homeWorld;
    }

    public List<String> getFilms() {
        return films;
    }

    public List<String> getStarships() {
        return starships;
    }

    public List<String> getVehicles() {
        return vehicles;
    }

    @JsonCreator
    public static StarWarsSwapiCharacter build(@JsonProperty("name") String name,
            @JsonProperty("birth_year") String birthYear, @JsonProperty("gender") String gender,
            @JsonProperty("hair_color") String hairColor, @JsonProperty("height") String height,
            @JsonProperty("mass") String mass, @JsonProperty("skin_color") String skinColor,
            @JsonProperty("homeworld") String homeWorld, @JsonProperty("films") List<String> films,
            @JsonProperty("starships") List<String> starships, @JsonProperty("vehicles") List<String> vehicles) {
        return new StarWarsSwapiCharacter(name, birthYear, gender, hairColor, height, mass, skinColor, homeWorld, films,
                starships, vehicles);
    }
}