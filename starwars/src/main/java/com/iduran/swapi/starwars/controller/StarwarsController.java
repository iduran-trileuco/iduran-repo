package com.iduran.swapi.starwars.controller;

import java.net.URI;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.iduran.swapi.starwars.exception.CharacterNotFoundException;
import com.iduran.swapi.starwars.mapper.StarwarsCharacterMapper;
import com.iduran.swapi.starwars.pojos.StarWarsCharacter;
import com.iduran.swapi.starwars.service.pojos.StarWarsSwapiCharacter;
import com.iduran.swapi.starwars.service.pojos.SwapiConveyance;
import com.iduran.swapi.starwars.service.pojos.SwapiFilm;
import com.iduran.swapi.starwars.service.pojos.SwapiHomeWorld;
import com.iduran.swapi.starwars.service.pojos.SwapiResponse;

@RestController
@RequestMapping(value = { "/swapi-proxy" })
public class StarwarsController {

    private static final String BASE_URL = "http://swapi.trileuco.com:1138/api";

    @RequestMapping("/")
    public String index() {
        return "Ivan te saluda, joven padawan!";
    }

    @RequestMapping("/person-info")
    public StarWarsCharacter findByName(@RequestParam String name) throws Exception {

        RestTemplate restTemplate = new RestTemplate();
        URI targetUrl = UriComponentsBuilder.fromUriString(BASE_URL) // Build the base link
                .path("/people/") // Add path
                .queryParam("search", URLDecoder.decode(name)) // Add one or more query params
                .build() // Build the URL
                .encode() // Encode any URI items that need to be encoded
                .toUri();
        SwapiResponse response = null;
        StarWarsSwapiCharacter character = null;
        List<SwapiFilm> films = new ArrayList<SwapiFilm>();
        List<SwapiConveyance> vehicles = new ArrayList<SwapiConveyance>();
        List<SwapiConveyance> starships = new ArrayList<SwapiConveyance>();
        SwapiHomeWorld homeWorld = null;
        // Find character
        try {
            response = restTemplate.getForObject(targetUrl, SwapiResponse.class);
            if (!CollectionUtils.isEmpty(response.getResults())) {
                character = response.getResults().get(0);

                // Get films
                for (String filmUrl : character.getFilms()) {
                    SwapiFilm film = restTemplate.getForObject(filmUrl, SwapiFilm.class);
                    films.add(film);
                }
                // Get vehicles
                for (String vehicleUrl : character.getVehicles()) {
                    SwapiConveyance vehicle = restTemplate.getForObject(vehicleUrl, SwapiConveyance.class);
                    vehicles.add(vehicle);
                }
                // Get starships
                for (String starshipUrl : character.getStarships()) {
                    SwapiConveyance starship = restTemplate.getForObject(starshipUrl, SwapiConveyance.class);
                    starships.add(starship);
                }

                // Get homeworld
                homeWorld = restTemplate.getForObject(character.getHomeWorld(), SwapiHomeWorld.class);

            } else {
                throw new HttpClientErrorException(HttpStatus.NOT_FOUND);
            }
        } catch (HttpClientErrorException e) {
            if (e.getStatusCode() == HttpStatus.NOT_FOUND) {
                throw new CharacterNotFoundException("Character with name:" + name + " not found in " + targetUrl);
            }
        }
        return StarwarsCharacterMapper.toStarwarsCharacter(character, films, vehicles, starships, homeWorld);
    }
}