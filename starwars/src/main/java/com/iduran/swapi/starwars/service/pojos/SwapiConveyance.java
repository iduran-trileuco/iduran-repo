package com.iduran.swapi.starwars.service.pojos;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SwapiConveyance {

    private final String name;
    private final Integer maxAtmospheringSpeed;

    public SwapiConveyance(String name, String maxAtmospheringSpeed) {
        super();
        this.name = name;
        if (StringUtils.isNumeric(maxAtmospheringSpeed)) {
            this.maxAtmospheringSpeed = Integer.valueOf(maxAtmospheringSpeed);
        } else {
            this.maxAtmospheringSpeed = 0;
        }
    }

    public String getName() {
        return name;
    }

    public Integer getMaxAtmospheringSpeed() {
        return maxAtmospheringSpeed;
    }

    @JsonCreator
    private static SwapiConveyance build(@JsonProperty("name") String name,
            @JsonProperty("max_atmosphering_speed") String maxAtmospheringSpeed) {
        return new SwapiConveyance(name, maxAtmospheringSpeed);
    }
}