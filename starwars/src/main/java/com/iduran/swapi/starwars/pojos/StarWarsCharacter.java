package com.iduran.swapi.starwars.pojos;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class StarWarsCharacter {

    private final String name;
    private final String birthYear;
    private final String gender;
    private final String planetName;
    private final String fastestVehicleDriven;
    private final List<Film> films;

    public StarWarsCharacter(String name, String birthYear, String gender, String planetName,
            String fastestVehicleDriven, List<Film> films) {
        super();
        this.name = name;
        this.birthYear = birthYear;
        this.gender = gender;
        this.planetName = planetName;
        this.fastestVehicleDriven = fastestVehicleDriven;
        this.films = films;
    }

    public String getName() {
        return name;
    }

    public String getBirthYear() {
        return birthYear;
    }

    public String getGender() {
        return gender;
    }

    public String getPlanetName() {
        return planetName;
    }

    public String getFastestVehicleDriven() {
        return fastestVehicleDriven;
    }

    public List<Film> getFilms() {
        return films;
    }
}
