package com.iduran.swapi.starwars.pojos;

import java.util.Date;

public class Film {

    private final String name;
    private final Date releaseDate;

    public Film(String name, Date releaseDate) {
        super();
        this.name = name;
        this.releaseDate = releaseDate;
    }

    public String getName() {
        return name;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }
}