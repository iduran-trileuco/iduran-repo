package com.iduran.swapi.starwars.service.pojos;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SwapiHomeWorld {
    private final String name;

    public SwapiHomeWorld(String name) {
        super();
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @JsonCreator
    private static SwapiHomeWorld build(@JsonProperty("name") String name) {
        return new SwapiHomeWorld(name);
    }
}