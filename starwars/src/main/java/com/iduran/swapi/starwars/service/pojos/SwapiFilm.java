package com.iduran.swapi.starwars.service.pojos;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SwapiFilm {

    private final String name;

    private final Date releaseDate;

    public SwapiFilm(String name, Date releaseDate) {
        super();
        this.name = name;
        this.releaseDate = releaseDate;
    }

    public String getName() {
        return name;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    @JsonCreator
    private static SwapiFilm build(@JsonProperty("title") String name, @JsonProperty("release_date") Date releaseDate) {
        return new SwapiFilm(name, releaseDate);
    }
}
