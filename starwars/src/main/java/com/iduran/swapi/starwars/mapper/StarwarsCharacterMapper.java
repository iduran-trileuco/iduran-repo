package com.iduran.swapi.starwars.mapper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.springframework.util.CollectionUtils;

import com.iduran.swapi.starwars.pojos.Film;
import com.iduran.swapi.starwars.pojos.StarWarsCharacter;
import com.iduran.swapi.starwars.service.pojos.StarWarsSwapiCharacter;
import com.iduran.swapi.starwars.service.pojos.SwapiConveyance;
import com.iduran.swapi.starwars.service.pojos.SwapiFilm;
import com.iduran.swapi.starwars.service.pojos.SwapiHomeWorld;

public class StarwarsCharacterMapper {

    public static StarWarsCharacter toStarwarsCharacter(StarWarsSwapiCharacter swapiCharacter, List<SwapiFilm> films,
            List<SwapiConveyance> vehicles, List<SwapiConveyance> starShips, SwapiHomeWorld homeWorld) {
        SwapiConveyance fastestVehicleDriven = getFastestVehicle(vehicles, starShips);

        return new StarWarsCharacter(swapiCharacter.getName(), swapiCharacter.getBirthYear(),
                swapiCharacter.getGender(), homeWorld.getName(),
                fastestVehicleDriven != null ? fastestVehicleDriven.getName() : null, toFilms(films));
    }

    public static List<Film> toFilms(List<SwapiFilm> films) {
        List<Film> results = new ArrayList<Film>();
        if (!CollectionUtils.isEmpty(films)) {
            for (SwapiFilm film : films) {
                Film result = new Film(film.getName(), film.getReleaseDate());
                results.add(result);
            }
        }
        return results;
    }

    private static SwapiConveyance getFastestVehicle(List<SwapiConveyance> vehicles, List<SwapiConveyance> starships) {

        SwapiConveyance fastestVehicle = null;
        SwapiConveyance fastestStarship = null;

        if (!CollectionUtils.isEmpty(vehicles)) {
            fastestVehicle = Collections.max(vehicles, new Comparator<SwapiConveyance>() {
                @Override
                public int compare(SwapiConveyance o1, SwapiConveyance o2) {
                    return o1.getMaxAtmospheringSpeed().compareTo(o2.getMaxAtmospheringSpeed());
                }
            });
        }
        if (!CollectionUtils.isEmpty(starships)) {
            fastestStarship = Collections.max(starships, new Comparator<SwapiConveyance>() {
                @Override
                public int compare(SwapiConveyance o1, SwapiConveyance o2) {
                    return o1.getMaxAtmospheringSpeed().compareTo(o2.getMaxAtmospheringSpeed());
                }
            });
        }
        if (fastestVehicle != null) {
            if (fastestStarship != null) {
                return fastestVehicle.getMaxAtmospheringSpeed() >= fastestStarship.getMaxAtmospheringSpeed()
                        ? fastestVehicle
                        : fastestStarship;
            } else {
                return fastestVehicle;
            }
        } else {
            if (fastestStarship != null) {
                return fastestStarship;
            } else {
                return null;
            }
        }
    }
}